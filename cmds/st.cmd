# -----------------------------------------------------------------------------
# pace5000 startup cmd
# -----------------------------------------------------------------------------
require pace5000

# -----------------------------------------------------------------------------
# Default environment variables
# -----------------------------------------------------------------------------
epicsEnvSet("IPADDR",   "172.30.244.78")
epicsEnvSet("IPPORT",   "5052")
epicsEnvSet("SYS",      "SES-PREMP:")
epicsEnvSet("DEV",      "Pctrl-PACE5000-001:")
epicsEnvSet("PREFIX",   "$(SYS)$(DEV)")


# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

# PACE 5000
iocshLoad("$(pace5000_DIR)pace5000.iocsh", "PREFIX=$(PREFIX), P=$(SYS), R=$(DEV), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")

iocInit()

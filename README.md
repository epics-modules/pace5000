# pace5000

European Spallation Source ERIC Site-specific EPICS module: pace5000

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/GE+Pace+5000+Pressure+Automated+Calibration+system)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/pace5000-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
